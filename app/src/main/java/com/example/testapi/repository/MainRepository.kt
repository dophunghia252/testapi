package com.example.testapi.repository

import com.example.testapi.api.Retrofit.Companion.api
import com.example.testapi.model.MCPEItem
import retrofit2.Retrofit
import javax.inject.Inject

class MainRepository @Inject constructor() {
    suspend fun getTopDownload() = api.getTopDownload()

    suspend fun searchItemByKeyword(search_text: String, page: Int) =
        api.searchItemByKeyword(search_text, page)

//    suspend fun insertItem(item: MCPEItem) = itemDAO.insertItem(item)
}