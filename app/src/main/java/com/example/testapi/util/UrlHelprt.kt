package com.example.testapi.util

object UrlHelprt {
    fun convertUrl(urlString: String): String =
        if (urlString.contains(Constant.BASE_URL, false)) {
            urlString
        } else {
            Constant.BASE_URL + "/" + urlString
        }
}