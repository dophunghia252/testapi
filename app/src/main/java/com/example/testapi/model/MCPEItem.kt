package com.example.testapi.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import androidx.room.Entity
import androidx.room.PrimaryKey
@Parcelize
@Entity(tableName = "mcpe")
data class MCPEItem(
    @PrimaryKey(autoGenerate = false)
    val id: Int?,
    val image: ArrayList<String>?,
    val name: String?,
    val time_create: String?,
    val version: String?,
    val download: Int?,
    val like: Int?,
    val description: String?,
    val file: String?,
    val type_file: String?,
    val name_file: String?,
    val coin: Int?,
    val category: ArrayList<CategoryItem>?
) : Parcelable