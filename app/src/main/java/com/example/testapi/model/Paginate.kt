package com.example.testapi.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Paginate(
    val total: Int?,
    val per_page: Int?,
    val current_page: Int?,
    val last_page: Int?,
    val next: Int?,
    val prev: Int?
) : Parcelable
