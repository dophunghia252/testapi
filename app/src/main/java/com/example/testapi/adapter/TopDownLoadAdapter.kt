package com.example.testapi.adapter


import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.style.TextAppearanceSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testapi.R
import com.example.testapi.model.MCPEItem
import com.example.testapi.util.UrlHelprt
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerDrawable
import kotlinx.android.synthetic.main.item_download_honrizolta.view.*
import java.util.*
import kotlin.collections.ArrayList

class TopDownLoadAdapter(var list: ArrayList<MCPEItem>, private val isFromFavorite: Boolean) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var clickItemListener: ((MCPEItem) -> Unit)? = null

    private var insertFavouriteItemListener: ((MCPEItem) -> Unit)? = null

    private var deleteFavouriteItemListener: ((MCPEItem) -> Unit)? = null

    private var queryText = ""
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_download_honrizolta, parent, false)
        return TopDownLoadAdapterViewHolder(view)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]
        (holder as TopDownLoadAdapterViewHolder).itemView.apply {
            val dataText = item.name
            if (queryText.isNotEmpty()) {
                val startPos = dataText?.toLowerCase(Locale.ROOT)?.indexOf(

                    queryText.toLowerCase(
                        Locale.ROOT
                    )
                )
                val endPos = startPos?.plus(queryText.length)
                if (startPos != -1) {
                    val spannable = SpannableString(dataText)
                    val colorStateList =
                        ColorStateList(arrayOf(intArrayOf()), intArrayOf(Color.BLUE))
                    val textAppearance =
                        TextAppearanceSpan(null, Typeface.BOLD, -1, colorStateList, null)
                    spannable.setSpan(
                        textAppearance,
                        startPos!!,
                        endPos!!,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    tvTitle.text = spannable
                }else {
                    tvTitle.text = dataText
                }

            }else {
                tvTitle.text = dataText
            }
            val shimmer = Shimmer.ColorHighlightBuilder()
                .setBaseColor(Color.parseColor("#F3F3F3"))
                .setBaseAlpha(1f)
                .setHighlightColor(Color.parseColor("#E7E7E7"))
                .setHighlightAlpha(1f)
                .setDropoff(50f)
                .build()

            val shimmerDrawable = ShimmerDrawable()
            shimmerDrawable.setShimmer(shimmer)
            if (item.image?.isNotEmpty() == true) {
                Glide.with(imgAvatar).load(UrlHelprt.convertUrl(item.image[0]))
                    .placeholder(shimmerDrawable)
                    .into(imgAvatar)
            }
//            tvDongCoins.text =item.coin.toString()
//            tvGiaTienCoins.text = item.coin.toString()
            tvDownload.text = item.download.toString()
            tvNumHeat.text = item.like.toString()
            setOnClickListener {
                clickItemListener?.let {
                    it(item)
                }
            }
            imgHeart.setOnClickListener {
                insertFavouriteItemListener?.let {
                    it(item)
                }
            }
        }

    }

   //
    override fun getItemCount(): Int {
        return list.size
    }
    fun onItemClick(listener: (MCPEItem) -> Unit) {
        this.clickItemListener = listener
    }


}
//


class TopDownLoadAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}
