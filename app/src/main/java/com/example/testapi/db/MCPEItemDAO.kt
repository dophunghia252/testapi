//package com.example.testapi.db
//
//import androidx.lifecycle.LiveData
//import androidx.room.*
//import com.example.testapi.model.MCPEItem
//
//@Dao
//interface MCPEItemDAO {
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insertItem(item: MCPEItem) : Long
//
//    @Query("SELECT * FROM mcpe")
//    fun getAllFavoriteItems(): LiveData<List<MCPEItem>>
//
//    @Delete
//    suspend fun deleteItem(item: MCPEItem)
//
//    @Query("SELECT EXISTS (SELECT 1 FROM mcpe WHERE id = :id)")
//    suspend fun exists(id: Int): Boolean
//}