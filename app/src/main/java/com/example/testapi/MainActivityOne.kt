package com.example.testapi

import android.app.Application
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ProcessLifecycleOwner
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyApplication : Application() ,LifecycleObserver {

    companion object {
        private lateinit var myApplication: MyApplication
        fun getApplication(): MyApplication = myApplication
    }

    override fun onCreate() {
        super.onCreate()
        myApplication = this
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }
}