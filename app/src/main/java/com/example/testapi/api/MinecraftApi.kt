package com.example.testapi.api

import com.example.testapi.model.MCPEResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface MinecraftApi {
    @GET("/api/v1/list-download")
 suspend fun getTopDownload(): Response<MCPEResponse>

    @POST("/api/v1/list-search")
    suspend fun searchItemByKeyword(
        @Query("search_text") search_text: String,
        @Query("page") page: Int
    ): Response<MCPEResponse>
 }


