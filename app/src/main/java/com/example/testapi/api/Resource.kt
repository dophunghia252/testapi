package com.example.testapi.api

sealed class Resource <T> (val data: T? = null, val message: String? = null) {

    class onSuccess<T>(data: T) : Resource<T>(data)

    class onError<T>(message: String, data: T? = null) : Resource<T>(data, message)

    class onLoading<T> : Resource<T>()

}