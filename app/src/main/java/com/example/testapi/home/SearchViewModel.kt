package com.example.testapi.home

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testapi.api.Resource
import com.example.testapi.model.MCPEItem
import com.example.testapi.model.MCPEResponse
import com.example.testapi.repository.MainRepository
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

class SearchViewModel @Inject constructor(private val repository: MainRepository) : ViewModel(),
    LifecycleObserver {
    val listMCPEState: MutableLiveData<Resource<MCPEResponse>> = MutableLiveData()
    var listMCPE: ArrayList<MCPEItem> = arrayListOf()




}