package com.example.testapi.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testapi.adapter.TopDownLoadAdapter
import com.example.testapi.R
import com.example.testapi.api.Resource
import com.example.testapi.model.MCPEItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import android.view.inputmethod.EditorInfo

import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.widget.SearchView
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var myItemAdapter: TopDownLoadAdapter
    private lateinit var listItemTopDownLoad: ArrayList<MCPEItem>
    private var timer = Timer()
    val viewModel: ViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.getTopDownload()
        initTopDownload()
        getTopDownload()
        onEvent()
        getSearch()

    }
    private fun onEvent() {
        searchView.setOnQueryTextListener(object : android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(p0: String?): Boolean {
                Log.d("AAA", "onQueryTextChange" + p0)
                Log.d("AAA", "onQueryTextSubmit: " + p0)
                timer.cancel()
                timer = Timer()
                timer.schedule(object : TimerTask() {
                    override fun run() {
                        viewModel.searchItem("" + p0, 1)
                    }
                },3000)
                return false
            }
        })
    }
    private fun getApi(){}

    private fun getSearch(){
        viewModel.listMCPEState.observe(this,{ response ->
            when(response) {
                is Resource.onSuccess -> {
                    response.data?.let { itemRes ->
                        listItemTopDownLoad.clear()
                        listItemTopDownLoad.addAll(itemRes.data)
                        myItemAdapter.notifyDataSetChanged()

                    }
                }
            }
        })
    }

    private fun initTopDownload() {
        listItemTopDownLoad = ArrayList()
        myItemAdapter = TopDownLoadAdapter(listItemTopDownLoad, false)
        rvTopDownLoad.apply {
            adapter = myItemAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }
    }
    private fun getTopDownload() {
        viewModel.listMCPEState.observe(this, { response ->
            when (response) {
                is Resource.onSuccess -> {
                    response.data?.let { itemRes ->
                        listItemTopDownLoad.addAll(itemRes.data)
                        myItemAdapter.notifyDataSetChanged()
                    }
                }
            }
        })

    }


}


