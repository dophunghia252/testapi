package com.example.testapi.home
import android.util.Log
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testapi.repository.MainRepository
import com.example.testapi.api.Resource

import com.example.testapi.model.MCPEItem
import com.example.testapi.model.MCPEResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject
@HiltViewModel

class ViewModel @Inject constructor(private val repository: MainRepository) : ViewModel() , LifecycleObserver {
    val listMCPEState: MutableLiveData<Resource<MCPEResponse>> = MutableLiveData()
    var listMCPES: ArrayList<MCPEItem> = arrayListOf()

    fun getTopDownload() = viewModelScope.launch {
        try {
            listMCPEState.postValue(Resource.onLoading())
            val response = repository.getTopDownload()
            listMCPEState.postValue(handleGetItemResponse(response))
        } catch (e: Exception) {
            listMCPEState.postValue(Resource.onError("Something wrong, please try again later (getTopDownload)"))
        }
    }

    private fun handleGetItemResponse(response: Response<MCPEResponse>): Resource<MCPEResponse> {
        if (response.isSuccessful) {
            response.body()?.let { result ->
                listMCPES = result.data
                Log.e("DATAAAAA", result.toString())
                return Resource.onSuccess(result)
            }
        }

        return Resource.onError(response.message())
    }
    fun searchItem(key: String, page: Int) = viewModelScope.launch {
        try {
            listMCPEState.postValue(Resource.onLoading())
            val response = repository.searchItemByKeyword(key, page)
            listMCPEState.postValue(handleListItemResponse(response))
        } catch (e: Exception) {
            listMCPEState.postValue(Resource.onError("Something wrong, please try again later"))
        }
    }
            private fun handleListItemResponse(response: Response<MCPEResponse>): Resource<MCPEResponse> {
            if (response.isSuccessful) {
            response.body()?.let { result ->
                listMCPES = result.data
                return Resource.onSuccess(result)
            }
        }
        return Resource.onError(response.message())
    }
}